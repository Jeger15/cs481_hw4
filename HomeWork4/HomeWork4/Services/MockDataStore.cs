﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork4.Models;

namespace HomeWork4.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "Mac & Cheese", Description="Easy Recipe for Mac & Cheese", Url ="https://spicysouthernkitchen.com/wp-content/uploads/velveeta-mac-and-cheese-22.jpg", Ingredient="1/2 c. (1 stick) butter, plus more for baking dish 1 lb. elbow macaroni 1/2 c. flour 5 c. whole milk 1 1/2 tsp. kosher salt Freshly ground black pepper 1 tsp. mustard powder 3 c. shredded cheddar 2 c. shredded Gruyère 1 1/2 c. grated Parmesan, divided 1 c. panko bread crumbs 3 tbsp. extra-virgin olive oil", Recipe="Cook macaroni according to the package directions. Drain. In a saucepan, melt butter or margarine over medium heat. Stir in enough flour to make a roux. Add milk to roux slowly, stirring constantly. Stir in cheeses, and cook over low heat until cheese is melted and the sauce is a little thick. Put macaroni in large casserole dish, and pour sauce over macaroni. Stir well. Melt butter or margarine in a skillet over medium heat. Add breadcrumbs and brown. Spread over the macaroni and cheese to cover. Sprinkle with a little paprika. Bake at 350 degrees F (175 degrees C) for 30 minutes. Serve."},
                new Item { Id = Guid.NewGuid().ToString(), Text = "Bacon & Spinach Stuffed Chicken", Description="Simple Bacon Spinach Chicken", Url ="https://drizzlemeskinny.com/wp-content/uploads/2015/01/DSCN5019edit.jpg", Ingredient="4 boneless skinless chicken breasts Kosher salt Freshly ground black pepper 4 oz. cream cheese, softened 1/2 c. frozen spinach, defrosted and drained 1/3 c. chopped artichoke hearts 1 c. shredded mozzarella, divided Pinch crushed red pepper flakes 4 strips bacon, cut into 4 strips 2 tbsp. extra-virgin olive oil ", Recipe="Preheat oven to 400°. Line a large baking sheet with foil. Make slits widthwise in chicken, being careful not to cut all the way through chicken. Season with salt and pepper. Place on prepared baking sheet. In a medium bowl, combine cream cheese, spinach, artichokes, and ½ cup of mozzarella. Season with salt, pepper, and a pinch of red pepper flakes. Fill every other slit with cream cheese mixture and fill remaining slits with a piece of bacon. Sprinkle remaining ½ cup mozzarella on top and drizzle with oil. Bake until chicken is cooked through and bacon is crispy, 35 minutes."},
                new Item { Id = Guid.NewGuid().ToString(), Text = "Roast Lamb", Description="Best Recipe for Roast Lamb", Url ="https://img.taste.com.au/97AitAJ6/w720-h480-cfill-q80/taste/2016/11/classic-roast-lamb-105516-1.jpeg", Ingredient="1 (2-lb.) boneless lamb shoulder roast, tied with butcher's twine 4 cloves garlic, minced 1 tbsp. freshly chopped rosemary 2 tsp. fresh thyme leaves 3 tbsp. extra-virgin olive oil, divided Kosher salt Freshly ground black pepper 2 lb. baby potatoes, halved if large", Recipe="Preheat oven to 450º and place oven rack in lower third of oven. In a small bowl, mix together garlic, rosemary, thyme, and 1 tablespoon oil and season generously with salt and pepper. Rub all over lamb. In a baking dish, toss potatoes with remaining oil and season with more salt and pepper. Place lamb on top of potatoes and roast until internal temperature reaches 145º, about 1 hour. Let rest 15 minutes, remove twine, then slice roast and serve."},
                new Item { Id = Guid.NewGuid().ToString(), Text = "Cheesy Chicken Tacos", Description="Do some Delicious Tacos", Url ="https://frankskraut.com/wp-content/uploads/2018/06/Cheesy-Krauted-Chicken-Tacos-1024x576.jpg", Ingredient="1 tbsp. vegetable oil 1 onion, chopped 1 tbsp. ground cumin Kosher salt Freshly ground black pepper 2 c. cooked shredded chicken 1 (4.5-oz.) can green chilis 1 c. salsa 1 (16-oz.) can refried beans 12 hard taco shells 1 1/2 c. shredded pepperjack Freshly chopped cilantro, for garnish", Recipe="Preheat oven to 375º. In a large skillet over medium heat, heat oil. Add onion and cook until tender, 6 minutes, then add cumin and season with salt and pepper. Stir until combined. Add chicken, green chilis, and salsa and stir until combined and heated through. In a baking dish, spread a thin layer of refried beans (to help the taco shells stand up!). Spoon remaining refried beans into the bottom of a taco shell and top with chicken mixture. Place in baking dish. Top all over with cheese and bake until cheese is melty, 10 minutes."},
                new Item { Id = Guid.NewGuid().ToString(), Text = "Garlic Butter Meatballs", Description="Pasta with Pesto & Meatballs", Url ="https://food.theffeed.com/wp-content/uploads/sites/7/2019/01/delish-garlic-butter-meatballs-with-zoodles-still002-15320967221-e1547324429160.jpg", Ingredient="1 lb. ground chicken 5 cloves garlic, minced and divided 1 large egg, beaten 1/2 c. freshly grated Parmesan, plus more for garnish2 tbsp.freshly chopped parsley 1 / 4 tsp.crushed red pepper flakes Kosher salt Freshly ground black pepper 2 tbsp.extra - virgin olive oil 4 tbsp.butter Juice of 1 / 2 a lemon 1 lb.zoodles", Recipe="In a large bowl mix together ground chicken, 2 garlic cloves, egg, Parmesan, parsley, and red pepper flakes. Season with salt and pepper then form into tablespoon sized meatballs. In a large skillet over medium heat, heat oil and cook meatballs until golden on all sides and cooked through, 10 minutes. Transfer to a plate and wipe out skillet with a paper towel. Melt butter in skillet then add remaining 3 garlic cloves and cook until fragrant, 1 minute. Add zoodles to skillet and toss in garlic butter then squeeze in lemon juice. Add meatballs back and heat just until warmed through. Garnish with more Parmesan to serve."},
                new Item { Id = Guid.NewGuid().ToString(), Text = "Fondue Savoyarde", Description="French Recipe For an Fondue", Url ="https://www.francetoday.com/wp-content/uploads/2012/01/1723-4676.original-696x457.jpg", Ingredient="4 oz. shredded Beaufort cheese 4 oz. shredded Emmental cheese 4 oz. shredded Comté cheese  garlic clove 1 glass of white Savoie wine, such as Apremont, or any light, delicate white wine Baguette bread cut into 1 inch cubes several hours before eating", Recipe="If you’re having the fondue at night, cut the bread into cubes in the morning. This way they will have time to dry out just enough to help them stay on the forks when dipped into the melted cheese. At serving time, cut the garlic clove in half and rub the inside of the fondue pot with it. Add the white wine and place the pot on the stove over medium heat.When the wine is hot, add the cheese by handfuls, stirring constantly. Do not add all the cheese at once. Stir the mixture until it is homogenous. Add pepper to taste and transfer the fondue pot from the stove to the burner on the table. Each guest sticks a cube of bread on their fork and dips it into the pot, being careful not to lose the bread."}
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}