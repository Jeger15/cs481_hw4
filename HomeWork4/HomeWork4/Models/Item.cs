﻿using System;

namespace HomeWork4.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Ingredient { get; set; }
        public string Recipe { get; set; }
    }
}