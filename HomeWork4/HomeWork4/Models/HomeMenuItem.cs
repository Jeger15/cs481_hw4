﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork4.Models
{
    public enum MenuItemType
    {
        Recipe,
        AboutMe,
        About
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
